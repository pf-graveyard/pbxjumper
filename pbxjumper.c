/* vim: set tabstop=4:softtabstop=4:shiftwidth=4:noexpandtab */

/* SPDX-License-Identifier: GPL-3.0-or-later */

#include <errno.h>
#include <getopt.h>
#include <glusterfs/api/glfs.h>
#include <pfcq.h>
#include <pfgfq.h>
#include <pfpthq.h>
#include <regex.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/epoll.h>
#include <sysexits.h>

#ifdef _PBXJUMPER_USE_FANOTIFY
#include <sys/fanotify.h>
#else /* _PBXJUMPER_USE_FANOTIFY */
#include <sys/inotify.h>
#endif /* _PBXJUMPER_USE_FANOTIFY */

#define PBX_FILENAME_REGEX		"^((agent|dial|autodial)_[0-9]{14}_[0-9]+\\.[0-9]+\\.WAV)$"
#define PBX_SOURCE_DELIMITER	"."
#define PBX_QUALIFIER_DELIMITER	"_"
#define PBX_DATETIME_FORMAT		"%d%m%Y%H%M%S"
#define FORCED_MOVE_AGE			(5 * 60)
#define EMPTY_WAV_SIZE			60

#ifdef _PBXJUMPER_USE_FANOTIFY
#define EVENTS_BUFFER_SIZE		(8192 * sizeof(struct fanotify_event_metadata))
#else /* _PBXJUMPER_USE_FANOTIFY */
#define EVENTS_BUFFER_SIZE		(8192 * (sizeof(struct inotify_event) + NAME_MAX + 1))
#endif /* _PBXJUMPER_USE_FANOTIFY */

#ifndef _PBXJUMPER_USE_FANOTIFY
#define IN_EVENT_DATA_LEN (sizeof(struct inotify_event))
#define IN_EVENT_NEXT(event, length) \
	((length) -= (event)->len, \
	(struct inotify_event*)(((char *)(event)) + \
	(event)->len))
#define IN_EVENT_OK(event, length) \
	((long)(length) >= (long)IN_EVENT_DATA_LEN && \
	(long)(event)->len >= (long)IN_EVENT_DATA_LEN && \
	(long)(event)->len <= (long)(length))
#endif /* _PBXJUMPER_USE_FANOTIFY */

typedef struct notify_event_context
{
#ifdef _PBXJUMPER_USE_FANOTIFY
	int fd;
#else /* _PBXJUMPER_USE_FANOTIFY */
	char* localpath;
	char* filename;
#endif /* _PBXJUMPER_USE_FANOTIFY */
	int mask;
#ifndef _PBXJUMPER_USE_FANOTIFY
	int padding; /* struct padding */
#endif /* _PBXJUMPER_USE_FANOTIFY */
} notify_event_context_t;

static char* monitor_path = NULL;
static char* glfs_directory = NULL;
#ifdef _PBXJUMPER_USE_FANOTIFY
static const uint64_t event_mask = FAN_CLOSE_WRITE | FAN_ONDIR | FAN_EVENT_ON_CHILD;
#else /* _PBXJUMPER_USE_FANOTIFY */
static const uint64_t event_mask = IN_CLOSE_WRITE;
#endif /* _PBXJUMPER_USE_FANOTIFY */
static glfs_t* fs = NULL;
static volatile sig_atomic_t should_exit = 0;
static pfpthq_pool_t* pool;

static void store_file(const char* _path, const char* _filename)
{
	int read_size = 0;
	int day = 0;
	int month = 0;
	int year = 0;
	int local_fd = -1;
	unsigned long long written = 0;
	glfs_fd_t *remote_fd = NULL;
	regex_t regex;
	char* source = NULL;
	char* source_qualifier = NULL;
	char* source_id = NULL;
	char* source_extension = NULL;
	char* qualifier = NULL;
	char* qualifier_name = NULL;
	char* qualifier_datetime = NULL;
	char* qualifier_timestamp = NULL;
	char str_day[3];
	char str_month[3];
	char str_year[5];
	char read_buffer[IO_CHUNK_SIZE];
	char str_year_path[PATH_MAX];
	char str_month_path[PATH_MAX];
	char str_day_path[PATH_MAX];
	char str_file_path[PATH_MAX];
	struct tm qualifier_tm;
	struct stat st;

	pfcq_zero(&qualifier_tm, sizeof(struct tm));
	pfcq_zero(&st, sizeof(struct stat));
	pfcq_zero(&regex, sizeof(regex_t));
	pfcq_zero(read_buffer, IO_CHUNK_SIZE);
	pfcq_zero(str_year_path, PATH_MAX);
	pfcq_zero(str_month_path, PATH_MAX);
	pfcq_zero(str_day_path, PATH_MAX);
	pfcq_zero(str_file_path, PATH_MAX);
	pfcq_zero(str_day, 3);
	pfcq_zero(str_month, 3);
	pfcq_zero(str_year, 5);

	if (unlikely(lstat(_path, &st) == -1))
	{
		warning("lstat");
		goto lfree;
	}

	if (unlikely(st.st_size == EMPTY_WAV_SIZE))
	{
		if (unlikely(unlink(_path) == -1))
			warning("unlink");
		else
			debug("Removed empty WAV file %s\n", _filename);
		goto lfree;
	}

	verbose("Storing %s file\n", _filename);
	source = strdupa(_filename);										// Example: dial_06102014152459_1412598298.75856.WAV
	if (unlikely(!source))
	{
		warning("strdupa");
		goto lfree;
	}
	if (unlikely(regcomp(&regex, PBX_FILENAME_REGEX, REG_EXTENDED)))	// Verified by regex
	{
		warning("regcomp");
		goto lfree;
	}
	if (unlikely(regexec(&regex, source, 0, NULL, 0)))
	{
		warning("regexec");
		goto lfree;
	}
	regfree(&regex);
	source_qualifier = strsep(&source, PBX_SOURCE_DELIMITER);			// dial_06102014152459_1412598298
	if (unlikely(!source_qualifier))
	{
		warning("strsep");
		goto lfree;
	}
	source_id = strsep(&source, PBX_SOURCE_DELIMITER);					// 75856
	if (unlikely(!source_id))
	{
		warning("strsep");
		goto lfree;
	}
	source_extension = strsep(&source, PBX_SOURCE_DELIMITER);			// WAV
	if (unlikely(!source_extension))
	{
		warning("strsep");
		goto lfree;
	}
	qualifier = strdupa(source_qualifier);								// Example: dial_06102014152459_1412598298
	if (unlikely(!qualifier))
	{
		warning("strdupa");
		goto lfree;
	}
	qualifier_name = strsep(&qualifier, PBX_QUALIFIER_DELIMITER);		// dial
	if (unlikely(!qualifier_name))
	{
		warning("strsep");
		goto lfree;
	}
	qualifier_datetime = strsep(&qualifier, PBX_QUALIFIER_DELIMITER);	// 06102014152459
	if (unlikely(!qualifier_datetime))
	{
		warning("strsep");
		goto lfree;
	}
	qualifier_timestamp = strsep(&qualifier, PBX_QUALIFIER_DELIMITER);	// 1412598298
	if (unlikely(!qualifier_timestamp))
	{
		warning("strsep");
		goto lfree;
	}
	if (unlikely(!strptime(qualifier_datetime, PBX_DATETIME_FORMAT, &qualifier_tm)))
	{
		warning("strptime");
		goto lfree;
	}
	day = qualifier_tm.tm_mday;
	month = qualifier_tm.tm_mon + 1;
	year = qualifier_tm.tm_year + 1900;
	if (unlikely(snprintf(str_day, 3, "%02d", day) < 0))
		panic("snprintf");
	if (unlikely(snprintf(str_month, 3, "%02d", month) < 0))
		panic("snprintf");
	if (unlikely(snprintf(str_year, 5, "%04d", year) < 0))
		panic("snprintf");

	if (unlikely(snprintf(str_year_path, PATH_MAX, "%s/%s", glfs_directory, str_year) < 0))
		panic("snprintf");
	if (unlikely(snprintf(str_month_path, PATH_MAX, "%s/%s/%s", glfs_directory, str_year, str_month) < 0))
		panic("snprintf");
	if (unlikely(snprintf(str_day_path, PATH_MAX, "%s/%s/%s/%s", glfs_directory, str_year, str_month, str_day) < 0))
		panic("snprintf");
	if (unlikely(snprintf(str_file_path, PATH_MAX, "%s/%s/%s/%s/%s", glfs_directory, str_year, str_month, str_day, _filename) < 0))
		panic("snprintf");
	glfs_mkdir_safe(fs, str_year_path, CHMOD_755);
	glfs_mkdir_safe(fs, str_month_path, CHMOD_755);
	glfs_mkdir_safe(fs, str_day_path, CHMOD_755);

	local_fd = open(_path, O_RDONLY | O_LARGEFILE);
	if (unlikely(local_fd == -1))
	{
		warning("open");
		goto lfree;
	}
	remote_fd = glfs_creat(fs, str_file_path, O_CREAT | O_WRONLY | O_TRUNC, CHMOD_644);
	if (unlikely(!remote_fd))
	{
		warning("glfs_creat");
		goto lfree;
	}
	while ((read_size = read(local_fd, read_buffer, IO_CHUNK_SIZE)) > 0)
		written += glfs_write(remote_fd, read_buffer, read_size, 0);

	if (unlikely(unlink(_path) == -1))
		warning("unlink");

	debug("Wrote %llu bytes of %s file\n", written, _filename);

lfree:
	if (likely(local_fd != -1))
		if (unlikely(close(local_fd) == -1))
			warning("close");
	if (likely(remote_fd))
		if (unlikely(glfs_close(remote_fd)))
			warning("glfs_close");

	return;
}

static void cleaner_thread(union sigval _data)
{
	(void)_data;
	DIR* dh = NULL;
	time_t check_time = time(NULL);
	char buf[DENTRY_BUFFER_SIZE];
	char path[PATH_MAX];
	struct dirent* entry = NULL;
	struct stat st;

	pfcq_zero(buf, DENTRY_BUFFER_SIZE);
	pfcq_zero(&st, sizeof(struct stat));

	debug("%s\n", "Cleaner timer thread tick");

	dh = opendir(monitor_path);
	if (unlikely(!dh))
	{
		warning("opendir");
		goto out;
	}
	while (readdir_r(dh, (struct dirent*)buf, &entry), entry)
	{
		if (unlikely(strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0))
			continue;

		pfcq_zero(path, PATH_MAX);
		if (unlikely(snprintf(path, PATH_MAX, "%s/%s", monitor_path, entry->d_name) < 0))
		{
			warning("snprintf");
			continue;
		}

		if (unlikely(lstat(path, &st) == -1))
		{
			warning("lstat");
			continue;
		}
		if (check_time - st.st_mtime > FORCED_MOVE_AGE && !pfcq_isopened(path))
		{
			verbose("Moving %s forcely due to timeout\n", entry->d_name);
			store_file(path, entry->d_name);
		}
	}
	if (unlikely(closedir(dh) == -1))
		warning("closedir");

out:
	return;
}

static void* event_worker(void* _data)
{
	char* filename = NULL;
	char path[PATH_MAX];

	pfcq_zero(path, PATH_MAX);

	notify_event_context_t* data = _data;

	if (unlikely(!data))
	{
		warning("NULL");
		goto out;
	}

#ifdef _PBXJUMPER_USE_FANOTIFY
	if (likely(data->mask & FAN_CLOSE_WRITE))
#else /* _PBXJUMPER_USE_FANOTIFY */
	if (likely(data->mask & IN_CLOSE_WRITE))
#endif /* _PBXJUMPER_USE_FANOTIFY */
	{
#ifdef _PBXJUMPER_USE_FANOTIFY
		if (unlikely(snprintf(path, PATH_MAX, "%s", pfcq_get_file_path_from_fd(data->fd, path, PATH_MAX)) < 0))
			panic("snprintf");
		if (unlikely(strlen(path) == 0))
		{
			warning("pfcq_get_file_path_from_fd");
			goto lfree;
		}
		filename = basename(path);
		if (unlikely(!filename))
		{
			warning("basename");
			goto lfree;
		}
#else /* _PBXJUMPER_USE_FANOTIFY */
		filename = data->filename;
		if (unlikely(snprintf(path, PATH_MAX, "%s/%s", data->localpath, data->filename) < 0))
			panic("snprintf");
#endif /* _PBXJUMPER_USE_FANOTIFY */
		store_file(path, filename);
	}

#ifdef _PBXJUMPER_USE_FANOTIFY
lfree:
	if (unlikely(close(data->fd) == -1))
		warning("close");
#else /* _PBXJUMPER_USE_FANOTIFY */
	pfcq_free(data->localpath);
	pfcq_free(data->filename);
	data->localpath = NULL;
	data->filename = NULL;
#endif /* _PBXJUMPER_USE_FANOTIFY */
	pfcq_free(data);

out:
	pfpthq_dec(pool);

	return NULL;
}

static void sigall_handler(int _signo)
{
	(void)_signo;

	if (likely(!should_exit))
		should_exit = 1;

	return;
}

int main(int argc, char** argv)
{
	ssize_t notify_event_length = 0;
	int notify_fd = 0;
#ifndef _PBXJUMPER_USE_FANOTIFY
	int watch_fd = 0;
#endif /* _PBXJUMPER_USE_FANOTIFY */
	int glfs_port = GLFS_DEFAULT_PORT;
	int opts = 0;
	int daemonize = 0;
	int epoll_fd = 0;
	int epoll_count = 0;
	int be_verbose = 0;
	int do_debug = 0;
	int use_syslog = 0;
	int threads = -1;
	pthread_t id;
	timer_t timer_id;
	struct sigaction epoll_sigaction;
	struct epoll_event epoll_event;
	struct epoll_event epoll_events[EPOLL_MAXEVENTS];
    struct itimerspec timer_time;
    struct sigevent timer_event;
	sigset_t epoll_sigmask_new;
	sigset_t epoll_sigmask_old;
	char notify_buffer[EVENTS_BUFFER_SIZE];
	char* glfs_protocol = NULL;
	char* glfs_server = NULL;
	char* glfs_volume = NULL;
	char* glfs_log = NULL;

	struct option longopts[] = {
		{"gl-protocol",		required_argument,	NULL, 'o'},
		{"gl-server",		required_argument,	NULL, 's'},
		{"gl-port",			required_argument,	NULL, 'p'},
		{"gl-volume",		required_argument,	NULL, 'v'},
		{"gl-directory",	required_argument,	NULL, 'i'},
		{"source",			required_argument,	NULL, 'u'},
		{"threads",			required_argument,	NULL, 't'},
		{"daemonize",		no_argument,		NULL, 'd'},
		{"verbose",			no_argument,		NULL, 'a'},
		{"debug",			no_argument,		NULL, 'e'},
		{"syslog",			no_argument,		NULL, 'y'},
		{0, 0, 0, 0}
	};

	pfcq_zero(&id, sizeof(pthread_t));
	pfcq_zero(notify_buffer, EVENTS_BUFFER_SIZE);
	pfcq_zero(&timer_id, sizeof(timer_t));
	pfcq_zero(&epoll_sigaction, sizeof(struct sigaction));
	pfcq_zero(&timer_time, sizeof(struct itimerspec));
	pfcq_zero(&timer_event, sizeof(struct sigevent));
	pfcq_zero(&epoll_event, sizeof(struct epoll_event));
	pfcq_zero(epoll_events, EPOLL_MAXEVENTS * sizeof(struct epoll_event));
	pfcq_zero(&epoll_sigmask_new, sizeof(sigset_t));
	pfcq_zero(&epoll_sigmask_old, sizeof(sigset_t));

	while ((opts = getopt_long(argc, argv, "ospviutaedy", longopts, NULL)) != -1)
		switch (opts)
		{
			case 'o':
				glfs_protocol = strdupa(optarg);
				break;
			case 's':
				glfs_server = strdupa(optarg);
				break;
			case 'p':
				if (unlikely(!pfcq_isnumber(optarg)))
					panic("Wrong GlusterFS server port");
				glfs_port = atoi(optarg);
				break;
			case 'v':
				glfs_volume = strdupa(optarg);
				break;
			case 'i':
				glfs_directory = strdupa(optarg);
				break;
			case 'u':
				monitor_path = strdupa(optarg);
				break;
			case 't':
				if (unlikely(!pfcq_isnumber(optarg)))
					panic("Wrong threads count");
				threads = atoi(optarg);
				break;
			case 'a':
				be_verbose = 1;
				break;
			case 'e':
				do_debug = 1;
				break;
			case 'd':
				daemonize = 1;
				break;
			case 'y':
				use_syslog = 1;
				break;
			default:
				panic("Unknown option occurred");
				break;
		}

	pfcq_debug_init(be_verbose, do_debug, use_syslog);
	glfs_log = strdupa(use_syslog ? DEV_NULL : DEV_STDERR);

	if (daemonize)
		if (unlikely(daemon(0, 0) != 0))
			panic("daemon");

	pool = pfpthq_init("workers", threads);

	if (unlikely(!glfs_protocol))
		glfs_protocol = strdupa(GLFS_DEFAULT_PROTOCOL);
	if (unlikely(!glfs_server))
		panic("No GlusterFS server specified!");
	if (unlikely(!glfs_volume))
		panic("No GlusterFS volume specified!");
	if (unlikely(!glfs_directory))
		panic("No GlusterFS directory specified!");
	if (unlikely(!monitor_path))
		panic("No files source specified!");

	fs = glfs_new(glfs_volume);
	if (unlikely(!fs))
		panic("glfs_new");
	if (unlikely(glfs_set_logging(fs, glfs_log, GLFS_DEFAULT_VERBOSITY)))
		panic("glfs_set_logging");
	if (unlikely(glfs_set_volfile_server(fs, glfs_protocol, glfs_server, glfs_port)))
		panic("glfs_set_volfile_server");
	if (unlikely(glfs_init(fs)))
		panic("glfs_init");

#ifdef _PBXJUMPER_USE_FANOTIFY
	debug_simple("Using fanotify API\n");
	notify_fd = fanotify_init(FAN_CLOEXEC, O_RDONLY | O_CLOEXEC | O_LARGEFILE);
	if (unlikely(notify_fd < 0))
		panic("fanotify_init");
	if (unlikely(fanotify_mark(notify_fd, FAN_MARK_ADD, event_mask, -1, monitor_path) < 0))
		panic("fanotify_mark");
#else /* _PBXJUMPER_USE_FANOTIFY */
	debug("%s\n", "Using inotify API");
	if (unlikely((notify_fd = inotify_init()) < 0))
		panic("inotify_init");
	if (unlikely((watch_fd = inotify_add_watch(notify_fd, monitor_path, event_mask)) == -1))
		panic("inotify_add_watch");
#endif /* _PBXJUMPER_USE_FANOTIFY */

	epoll_sigaction.sa_handler = sigall_handler;
	if (unlikely(sigemptyset(&epoll_sigaction.sa_mask) != 0))
		panic("sigemptyset");
	epoll_sigaction.sa_flags = 0;
	if (unlikely(sigaction(SIGTERM, &epoll_sigaction, NULL) != 0))
		panic("sigaction");
	if (unlikely(sigaction(SIGINT, &epoll_sigaction, NULL) != 0))
		panic("sigaction");
	if (unlikely(sigemptyset(&epoll_sigmask_new) != 0))
		panic("sigemptyset");
	if (unlikely(sigaddset(&epoll_sigmask_new, SIGTERM) != 0))
		panic("sigaddset");
	if (unlikely(sigaddset(&epoll_sigmask_new, SIGINT) != 0))
		panic("sigaddset");
	if (unlikely(pthread_sigmask(SIG_BLOCK, &epoll_sigmask_new, &epoll_sigmask_old) != 0))
		panic("pthread_sigmask");

	timer_event.sigev_notify = SIGEV_THREAD;
	timer_event.sigev_notify_function = cleaner_thread;
	timer_event.sigev_notify_attributes = NULL;

	timer_time.it_value.tv_sec = 5;
	timer_time.it_value.tv_nsec = 0;
	timer_time.it_interval.tv_sec = 5;
	timer_time.it_interval.tv_nsec = 0;

	if (unlikely(timer_create(CLOCK_MONOTONIC, &timer_event, &timer_id) == -1))
		panic("timer_create");
	if (unlikely(timer_settime(timer_id, 0, &timer_time, NULL) == -1))
		panic("timer_settime");

	epoll_fd = epoll_create1(0);
	if (unlikely(epoll_fd == -1))
		panic("epoll_create");
	epoll_event.data.fd = notify_fd;
	epoll_event.events = EPOLLIN;
	if (unlikely(epoll_ctl(epoll_fd, EPOLL_CTL_ADD, notify_fd, &epoll_event) == -1))
		panic("epoll_ctl");

	for (;;)
	{
		epoll_count = epoll_pwait(epoll_fd, epoll_events, EPOLL_MAXEVENTS, -1, &epoll_sigmask_old);
		if (unlikely(epoll_count == -1))
		{
			if (likely(errno == EINTR))	// shutdown gracefully
			{
				if (likely(should_exit))
				{
					verbose("%s\n", "Caught terminate signal, attempting to exit gracefully...");
					goto lfree;
				} else
					continue;
			} else
			{
				warning("epoll_pwait");
				continue;
			}
		} else
		{
			for (int i = 0; i < epoll_count; i++)
			{
				if (likely(epoll_events[i].data.fd == notify_fd && (epoll_events[i].events & EPOLLIN)))
				{
					if (likely((notify_event_length = read(notify_fd, notify_buffer, EVENTS_BUFFER_SIZE)) > 0))
					{
#ifdef _PBXJUMPER_USE_FANOTIFY
						struct fanotify_event_metadata* notify_metadata = (struct fanotify_event_metadata*)notify_buffer;
						while (FAN_EVENT_OK(notify_metadata, notify_event_length))
#else /* _PBXJUMPER_USE_FANOTIFY */
						struct inotify_event* notify_metadata = (struct inotify_event*)notify_buffer;
						while (IN_EVENT_OK(notify_metadata, notify_event_length))
#endif /* _PBXJUMPER_USE_FANOTIFY */
						{
							notify_event_context_t* new_context = pfcq_alloc(sizeof(notify_event_context_t));
#ifdef _PBXJUMPER_USE_FANOTIFY
							new_context->fd = notify_metadata->fd;
#else /* _PBXJUMPER_USE_FANOTIFY */
							new_context->filename = pfcq_strdup(notify_metadata->name);
							new_context->localpath = pfcq_strdup(monitor_path);
#endif /* _PBXJUMPER_USE_FANOTIFY */
							new_context->mask = notify_metadata->mask;
							pfpthq_inc(pool, &id, "worker", event_worker, (void*)new_context);
#ifdef _PBXJUMPER_USE_FANOTIFY
							notify_metadata = FAN_EVENT_NEXT(notify_metadata, notify_event_length);
#else /* _PBXJUMPER_USE_FANOTIFY */
							notify_metadata = IN_EVENT_NEXT(notify_metadata, notify_event_length);
#endif /* _PBXJUMPER_USE_FANOTIFY */
						}
					}
				} else
				{
					warning("epoll_pwait");
					continue;
				}
			}
		}
	}

lfree:
	pfpthq_wait(pool);

	if (unlikely(glfs_fini(fs)))
		panic("glfs_fini");

	pfpthq_done(pool);

#ifdef _PBXJUMPER_USE_FANOTIFY
	if (unlikely(fanotify_mark(notify_fd, FAN_MARK_REMOVE, event_mask, -1, monitor_path) < 0))
		panic("fanotify_mark");
#else /* _PBXJUMPER_USE_FANOTIFY */
	if (unlikely(inotify_rm_watch(notify_fd, watch_fd) == -1))
		panic("inotify_rm_watch");
#endif /* _PBXJUMPER_USE_FANOTIFY */
	if (unlikely(close(notify_fd) == -1))
		warning("close");

	if (unlikely(timer_delete(timer_id) == -1))
		panic("timer_delete");

	verbose("%s\n", "Bye.");

	pfcq_debug_done();

	exit(EX_OK);
}

