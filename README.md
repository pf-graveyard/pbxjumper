pbxjumper
=========

Description
-----------

Daemon to store Asterisk recordings to GlusterFS share.

Protocol
--------

pbxjumper monitors source folder for new Asterisk recordings and moves them to GlusterFS
share after closing recording file (hanging up Asterisk call).

### Basics

pbxjumper deals with recordings conforming following file name syntax:

`^((agent|dial)_[0-9]{14}_[0-9]+\\.[0-9]+\\.WAV)$`

The following example matches the regex above:

`dial_06102014152459_1412598298.75856.WAV`

First part is either `dial` or `agent`.

Second part is date and time formatted by `%d%m%Y%H%M%S` rule.

Third part is UNIX timestamp.

Fourth part is unique call ID withing current Asterisk instance.

Fifth part is... well, it is considered to be only `WAV`.

### Typical scheme

* create RAM-disk (tmpfs) of sufficient size and mount it to, say, `/mnt/asterisk_mixmonitor`;
* change appropriate asterisk.conf file option to handle new mount point;
* launch pbxjumper;
* ???;
* enjoy moving Asterisk recordings from RAM-disk to GlusterFS share.

Note that recording files are stored to source with no subfolders (plain file list). Also note that
pbxjumper sorts recordings on GlusterFS share according to year/month/day rule using subfolders.
Sorting is handled automatically.

To handle different Asterisk and inotify/fanotify issues, pbxjumper implements special monitoring
timer that checks source each 5 seconds for stalled files. Stalled file is a file that is at least
5 minutes old (according to mtime) and that has no references to itself via file descriptors in /proc
(that means no process holds it opened). Such file is considered to be written completely and is handled
automatically as well.

Compiling
---------

### Prerequisites

* cmake (tested with 2.8.11)
* make (tested with GNU Make 3.81, 3.82)
* gcc (tested with 4.4.5, 4.7.2, 4.8.2)

### Compiling

Create `build` folder, chdir to it, then run

`cmake ..`

or

`cmake -DCMAKE_BUILD_TYPE=Debug ..`

to build app with debug info. Then just type `make`.

To compile pbxjumper against fanotify API (Linux kernel version 2.6.37+, so not suited
for CentOS 6.5) add the following flag:

`-D_PBXJUMPER_USE_FANOTIFY`

To use inotify API (sufficient for everyone) just compile as it is.

Usage
-----

The following arguments are supported:

* --gl-protocol=&lt;tcp|udp&gt; (optional, default "tcp") specifies transport protocol, used to connect to GlusterFS daemon;
* --gl-server=&lt;address&gt; (mandatory) specifies GlusterFS daemon address (hostname or IP);
* --gl-port=&lt;port&gt; (optional, default "24007") specifies GlusterFS daemon port;
* --gl-volume=&lt;name&gt; (mandatory) specifies GlusterFS Asterisk recordings volume;
* --gl-directory=&lt;name&gt; (mandatory) specifies GlusterFS Asterisk recordings root folder;
* --source=&lt;path&gt; (mandatory) specifies path to folder to grab recordings from (RAM-disk in example above);
* --threads=&lt;count&gt; (optional, default "4") specifies worker threads count (try not to specify more especially under high load);
* --debug (optional) enables verbose output;
* --daemonize (optional) enables daemonization (preferred way to run on server);
* --syslog (optional) logs everything to syslog instead of /dev/stderr.

Typical usage:

`pbxjumper --gl-server=host3.la.net.ua --gl-volume=asterisk --gl-directory=/ --source=/mnt/asterisk_mixmonitor --daemonize --syslog`

Distribution and Contribution
-----------------------------

No special license is provided. This software is considered to be confidential.

The following people are involved in development:

* Oleksandr Natalenko &lt;o.natalenko@lanet.ua&gt;

Mail them any suggestions, bugreports and comments.
